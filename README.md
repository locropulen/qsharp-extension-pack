# Microsoft Quantum Development Extension Pack

A collection of popular extensions that can help write, test and debug Quantum applications in Visual Studio Code.

## Extensions included

### Microsoft Quantum Development Kit Preview

The Quantum Development Kit contains the tools you'll need to build your own quantum computing programs and experiments. Assuming some experience with Visual Studio Code, beginners can write their first quantum program, and experienced researchers can quickly and efficiently develop new quantum algorithms.

### Jupyter Extension for Visual Studio Code

A Visual Studio Code extension that provides basic notebook support for language kernels that are supported in Jupyter Notebooks today. Many language kernels will work with no modification. To enable advanced features, modifications may be needed in the VS Code language extensions.

#### Working with Python

Whether you are on VS Code Stable or VS Code Insiders, if you would like to work with Python just make sure you're using the latest version of the Python Extension to enjoy the joint partnership of the Python and Juypter Extensions!

### Python extension for Visual Studio Code

A Visual Studio Code extension with rich support for the Python language (for all actively supported versions of the language: >=3.6), including features such as IntelliSense, linting, debugging, code navigation, code formatting, Jupyter notebook support, refactoring, variable explorer, test explorer, and more!

### Enhanced F# Language Features for Visual Studio Code

Part of the Ionide plugin suite. Read detailed documentation at Ionide docs page.

Version Installs Rating open collective backers open collective sponsors

You can support Ionide development on Open Collective.

Open Collective

Requirements
.NET 5.0 SDK - https://dotnet.microsoft.com/download/dotnet/5.0

VS Code C# plugin (optional, suggested) - Ionide's debugging capabilities relies on the debugger provided by Omnisharp team. To get it install C# extension from VS Code marketplace

### C# for Visual Studio Code (powered by OmniSharp)

Welcome to the C# extension for Visual Studio Code! This extension provides the following features inside VS Code:

Lightweight development tools for .NET Core.
Great C# editing support, including Syntax Highlighting, IntelliSense, Go to Definition, Find All References, etc.
Debugging support for .NET Core (CoreCLR). NOTE: Mono debugging is not supported. Desktop CLR debugging has limited support.
Support for project.json and csproj projects on Windows, macOS and Linux.
The C# extension is powered by OmniSharp.
